#import "template/boxes/colorboxes.typ": *
#import "@preview/codelst:2.0.0": sourcecode

#let utc_colors = (
   yellow: rgb(255, 217, 17),
   light_yellow: rgb(255,245,204),
   gray: rgb(89,89,89),
   light_gray: rgb(208,208,208),
   green: rgb(151,191,13),
   orange: rgb(236,117,46),
   blue: rgb(0,158,224),
)

#let title_page(
  uv_name: none,
  title_name: none,
  authors: none,
  date: datetime.today().display("[day] [month repr:long ] [year]"),
) = {

  grid(
    columns: (1fr, 1fr),
    [
    #image("template/logos/UTC.svg", width: 80%)
    ],
    [
      #align(right)[
        #image("template/logos/logoOriginal-1024x354.png", width: 80%)
      ]
    ],
  )

  align(horizon + center, {
    
    line(
      length: 100%,
      stroke: 0.8mm + utc_colors.yellow,
    )

    {
      set text(
        weight: "bold",
        size: 25pt,
      )
        
      if uv_name != none {
        uv_name
      }
  
      if uv_name != none and title_page != none {
        linebreak()
      }
      
      if title_name != none { 
        title_name
      }
    }
    line(length: 100%,
      stroke: 0.8mm + utc_colors.yellow,
    )

    {
      
      
      text(size: 17pt)[Réalisé par #authors.join(" et ")]
      linebreak()
      text(size: 14pt)[Encadrant de PR: Nicolas Piton]
      linebreak()
      v(5pt)
      text(size: 17pt)[#date]
    }
    
  })
  pagebreak()
}

#let french_months = (
  "January": "Janvier",
  "February": "Février",
  "March": "Mars",
  "April": "Avril",
  "May": "Mai",
  "June": "Juin",
  "July": "Juillet",
  "August": "Août",
  "September": "Septembre",
  "October": "Octobre",
  "November": "Novembre",
  "December": "Décembre",
)

#let avantages(body) = {
  admonition(color:"green", title:"Avantages",icon:"circle-plus-solid.svg",iconsize: 2em)[#body]
}

#let inconvenients(body) = {
  admonition(color:"red", title:"Incovénients",icon:"circle-minus-solid.svg",iconsize: 2em)[#body]
}

#let todo(body) = {
  set text(size: 15pt, fill : red)
  
  [TODO:]
  body
}

#let code(caption: none, body) = {
  figure(caption: caption)[
    #align(left)[
      #sourcecode(body)
    ]
  ]
}


#let template(
  uv_name: none,
  title_name: none,
  authors: ("Cécile Gaultier", "Yaniss Khima"),
  date: none,
  list_of_tables : true,
  body
) = {
  //setting up the meta-data
  set document(title: title_name, author: authors.join(", "))

  set text(lang: "fr", region: "fr", font: "Linux Libertine")

  if date == none {
    //Only english local is supported at the time
    let month = french_months.at(datetime.today().display("[month repr:long ]"))
    
    date = datetime.today().display("[day] " + month + " [year]")
  }
  
  title_page(
    uv_name: uv_name,
    title_name: title_name,
    authors: authors,
    date: date,
  )
  
  show par: set block(spacing: 1.3em)
  set par(
    first-line-indent: 1em,
    justify: true,
  )

  //Page config
  set page(
    
    header: locate(loc => {
      //Skip first page
      if loc.page() > 1 [
        #grid(
          columns: (1fr, 1fr, 1fr),
          rows: 60pt,
          {
            set align(left)
            smallcaps[#title_name]
          },
          {
            set align(center)
            image("template/logos/single_UTC.svg", width: 1.75cm)
          },
          {
            set align(right)
            [_#uv_name _]
          }
        )  
        
        #line(length: 100%,
        stroke: 1pt + black)
      ]
    }),

    footer: locate(loc => {
      if loc.page() > 1 [
        #line(length: 100%,
        stroke: 2pt + utc_colors.yellow)

        #grid(
          columns: (1fr, 1fr, 1fr),
          {
            set align(left)
            //UNUSED
          },
          {
            set align(center)
            counter(page).display()
          },
          {
            set align(right)
            //UNUSED
          }
        )    
      ]
    }),
  )
  
  show heading.where(level: 1): it => {
    underline(
      stroke: 1.5pt + utc_colors.yellow,
      it
    )
  }

  show heading.where(level: 2): it => {
    set text(fill: utc_colors.gray)
    it
  }

  show heading.where(level: 3): it => {
    set text(fill: utc_colors.gray)
    it
  }
  
  show heading.where(level: 4): it => {
    set text(fill: utc_colors.gray)
    // Si jamais tu veux mettre les titres 4 en inline c'est comme ca
    parbreak()
    text(12pt, weight: "bold", it.body + ".")
    linebreak()
    // Sinon tu return le titre normalement
    //it
    
  }

  set heading(numbering: "A.1.1.")  
  if list_of_tables {
    show outline.entry.where(level: 1): it => {  
        v(10pt, weak: true)
        strong(it)
    }

    show outline.entry: it => {
      if it.fill == none and it.level > 1 {
        // Create new outline entry without fill
        // (if-statement to prevent recursion)
        let params = it.fields()

        params.fill = repeat[#h(2pt).#h(2pt)]
        outline.entry(..params.values())
      } else {
        it
      }
    }

    ///
    /// Table of contents
    ///
    set par(first-line-indent: 0pt)

    outline(
      title: [Table des matières],
      depth: 3,
      indent: 12pt,
      fill: none,
    )

    pagebreak()
  }

  set list(marker: text(fill: utc_colors.yellow, size: 17pt)[•])

  //The content is rendered here
  body

}
