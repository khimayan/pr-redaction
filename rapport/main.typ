#import "../template.typ": *

#show: template.with(
  uv_name: "PR00",
  title_name: "Rapport de passation",
)

= Remerciements

Nous tenons à remercier Monsieur Nicolas Piton pour son encadrement et pour nous avoir fourni le matériel nécessaire lors de la réalisation du projet.

Nous remercions également Léo Naizin et Yaniss Khima qui ont rédigé le template Typst pour les rapports de l'UTC : https://gitlab.utc.fr/naizinle/typst-template-utc

= Introduction

Le Fablab UTC est une association étudiante créée en février 2014. Les locaux accueillent des machines de la plateforme de prototypage. Les étudiants et le personnel de l'UTC peuvent développer des projets personnels, scolaires, scientifiques, artistiques ou ludiques.

Il existe donc différents profils avec une expérience des outils variée. Afin de réduire la barrière technologique lors des projets et de répondre à des interrogations récurrentes, le Fablab a demandé à plusieurs groupes d'étudiants de rédiger des tutoriels pour enrichir leur documentation.

Notre groupe, composé de Yaniss Khima (GI5 Apprentissage) et de Cécile Gaultier (GI5 Apprentissage), a pu utiliser les tutoriels déjà existants comme références ou sources pour les réécrire. Vous pourrez les retrouver sur le site du Fablab : https://fablabutc.fr/documentation-tutoriels-electroniques

L'objectif était donc de rédiger 8 tutoriels (existants et nouveaux) en format PDF à destination de profils différents sur le domaine de l'embarqué pendant le semestre A23. Comme nous avons utilisé une nouvelle technologie pour la rédaction des tutoriels, nous avons également rédigé un rapport de passation.

= Analyse de l'existant

Nous avons commencé le projet en étudiant les tutoriels existants rédigés par les étudiants des années précédentes.

Pour commencer, les fichiers PDF souffrent d'une absence d'homogénéité entre eux, aussi bien pour la forme que pour le fond. Cela réduit considérablement l'expérience utilisateur car les informations ne sont pas présentées de la même façon et certains demandent plus d'efforts de compréhension. Par conséquent, la compréhension globale était plus difficile, surtout pour des néophytes. Certaines mises en forme, comme les listes d'avantages et d'inconvénients, étaient très pertinentes et nous avons décidé de nous en inspirer.

En outre, nous avions noté que les technologies utilisées pour leur rédaction étaient différentes (LaTeX et Microsoft Word) et nous n'avions pas accès à tous les fichiers sources. Il était donc compliqué de les mettre à jour.

Enfin, les logiciels comme MSWord et Latex ne séparent pas le fond et la forme lors de la rédaction des tutoriels. Il est en effet plus compliqué de généraliser la mise en forme, entraînant une hétérogénéité entre les tutoriels, et la prise en main des fichiers source est plus ardue.

#figure(
  image("images/tuto_relais.png", width: 70%),
  caption: [Extrait du tutoriel relais],
)
#figure(
  image("images/tuto_arduino.png", width: 70%),
  caption: [Extrait du tutoriel arduino],
)
= Architecture du Projet

== Choix Technologiques

Afin de remédier aux problèmes des tutoriels existants, nous avons opté pour l'utilisation de plusieurs logiciels spécifiques, chacun contribuant à améliorer significativement la qualité et la gestion de nos documents. Ils sont tous gratuits et faciles à prendre en main, afin de faciliter la transmission et la reprise par d'autres étudiants.

=== Typst

Nous avons choisi Typst pour améliorer les processus de rédaction de documents. Ce système ("markup-based typesetting") se concentre sur l'optimisation des flux de travail liés à l'écriture scientifique. La mise en forme et le fond sont séparés en deux fichiers distincts, ce qui garantit une présentation homogène et esthétique. Les fichiers sources sont facilement modifiables, ce qui permet d'être repris potentiellement par différents étudiants, même sans expérience en informatique, contrairement à LateX.

=== Fritzing

Fritzing a été choisi comme outil pour la création de schémas électroniques. Sa simplicité d'utilisation et sa capacité à générer des schémas clairs et visuellement attrayants en font un choix idéal pour illustrer nos tutoriels. Il est beaucoup plus facile de reproduire les branchements à partir de ces schémas plutôt que depuis une photographie, surtout pour les néophytes.

#figure(
  image("images/CAN.svg", width: 80%),
  caption: [Schéma Fritzing du module CAN à une Arduino],
)

=== Diagrams.net

Pour la création de diagrammes et d'organigrammes, nous avons opté pour Diagrams.net. Cette application en ligne offre une grande flexibilité pour représenter graphiquement les concepts et les processus, améliorant ainsi la compréhension visuelle du contenu.
#figure(
  image("images/Relay_open.svg", width: 30%),
  caption: [Diagramme d'un relai ouvert avec drawio],
)

=== Gitlab C.I

L'intégration continue est assurée par Gitlab C.I, facilitant la gestion et le suivi des versions de nos documents. Cette approche contribue à garantir la cohérence et la disponibilité des sources tout au long du processus pour les autres étudiants.

=== Python et Shell

Nous avons incorporé des scripts Python et Shell pour automatiser le processus de compilation grâce à notre expérience en GI. Cette automatisation facilite l'exportation des documents et favorise le développement continu, renforçant ainsi l'efficacité globale du projet. Ces choix technologiques combinés visent à résoudre les problèmes identifiés tout en optimisant la qualité, la maintenance et la collaboration au sein de notre initiative de documentation électronique.

== Arborescence du Projet

Pour l'organisation structurée du projet, nous avons pris la décision de segmenter l'ensemble des tutoriels en utilisant des dossiers dédiés. Chaque dossier regroupe une catégorie spécifique de tutoriels, telle que esp32, arduino, raspberry, etc., et chaque sous-dossier représente un projet individuel.

Chaque projet comprend les éléments suivants :
- Un fichier main.typ contenant le contenu principal du tutoriel.
- Un dossier "images" renfermant les fichiers .png et .svg utilisés dans le projet.
- Un dossier "ressources" contenant les fichiers drawio ou fritzing utilisés pour générer les images.

Chaque fichier main.typ importe en première ligne le fichier template.typ, situé à la racine du projet. Ce fichier template constitue l'emplacement central pour la mise en page principale et fait référence aux éléments présents dans le dossier "template", tels que les images ou les bibliothèques utilisées.

=== Template UTC

Le template employé pour les tutoriels est basé sur celui créé par Léo Naizin et Yaniss Khima, disponible à l'adresse suivante :
- https://gitlab.utc.fr/naizinle/typst-template-utc
Les informations détaillées sur ce template sont accessibles dans le référentiel ou dans le fichier readme.pdf, conçu pour orienter les débutants dans l'utilisation de Typst et du template.

=== Code-Box par LostPy

Nous avons repris la bibliothèque Code-Box, créée par LostPy, qui constitue une composante essentielle du projet. Le code source original est accessible à l'adresse suivante :
- https://gitlab.com/LostPy/lostpy-typst-templates
Nous avons développé un wrapper ("enveloppeur" en français : structure qui encapsule les fonctionnalités existantes pour les rendre plus faciles à utiliser) pour cette bibliothèque, dont voici le prototype de la fonction :
#code()[
```typ
#let code(caption: none, body)
```
]
Un exemple d'utilisation est présenté ci-dessous :
#code()[
````typ
#code()[
  ```cpp
  pinMode(pinInterrupteur, INPUT_PULLDOWN);
  // Configure la broche en mode pull-down
  ```
]
````
]

Nous pouvions ainsi ajouter facilement du code dans les tutoriels avec une coloration explicite selon le langage choisi. Ainsi, lors de leur lecture, l'utilisateur peut plus facilement identifier les fonctions ou les  paramètres utilisés, améliorant ainsi l'expérience utilisateur. 

=== ColorBox par Jomaway

Nous avons personnalisé la bibliothèque ColorBoxes développée par Jomaway, dont le code source est accessible ici:
- https://github.com/jomaway/typst-teacher-templates
Cette adaptation nous permet d'intégrer des petites boîtes colorées afin d'ajouter une dimension esthétique au code. Les catégories principalement utilisées sont `#info`, `#avantages` et `#inconvenients`, chacune avec son prototypage spécifique :

#code[
```typ
#let avantages(body)
#let inconvenients(body)
#let info(body, title: none, ..args)
```
]
Un exemple concret d'utilisation est illustré ci-dessous :
#code[
```typ
#avantages[- Coût abordable.]

#inconvenients[- Limité à une communication de 1 Mbit/s.]

#info(title: "Attention")[  
Si les 2 pins sont activés en même temps, il y a un risque de court-circuit!
]
```
]
Le code est ensuite traduit visuellement lors de la génération en pdf :

#avantages[- Coût abordable.]

#inconvenients[- Limité à une communication de 1 Mbit/s.]

#info(title: "Attention")[  
Si les 2 pins sont activés en même temps, il y a un risque de court-circuit!
]

Ces ajouts apportent une dimension visuelle et informative supplémentaire au code, facilitant ainsi la compréhension des avantages, inconvénients et informations importantes associées à chaque section.


== Déploiement continu
Le déploiement continu est intégré à notre processus de travail grâce au fichier `.gitlab-ci.yaml`

Dans la phase de construction, le script compile les fichiers "main.typ" en "main.pdf". Les PDF générés sont sauvegardés comme artefacts pour la phase suivante. La phase de déploiement crée un fichier compressé contenant tous les PDF, assurant une mise à jour continue des documents en réponse aux modifications apportées au dépôt.

La phase de compilation est réalisée par le script shell `build.sh`. Il itère sur les fichiers Typst, extrait les informations nécessaires, nettoie les noms de fichiers, puis utilise l'outil typst pour générer les fichiers PDF. Ce script permet une gestion fluide des balises uv_name et title_name, garantissant une sortie PDF organisée et cohérente.

L'utilisation de l'image Docker 123marvin123/typst est issue de Docker Hub à cette adresse :
- https://hub.docker.com/r/123marvin123/typst

Voici quelques raisons fondamentales justifiant ce choix :

=== Uniformité de l'Environnement de Compilation
L'image Docker spécifiée dans le fichier de configuration GitLab CI/CD garantit un environnement de compilation uniforme. Cette uniformité est cruciale pour éviter les variations entre les différents environnements de développement et s'assurer que la génération des documents Typst en PDF est cohérente, quel que soit l'endroit où le processus est déclenché.

=== Gestion Simplifiée des Dépendances
Docker encapsule l'ensemble des dépendances nécessaires pour exécuter le processus de compilation Typst. Cela simplifie considérablement la gestion des dépendances, réduisant le risque de conflits et de problèmes liés aux versions, et facilitant la reproduction du processus sur différents systèmes.

=== Portabilité et Reproductibilité
L'utilisation d'une image Docker standardise l'environnement d'exécution, améliorant ainsi la portabilité des scripts de compilation. Les développeurs peuvent exécuter les mêmes scripts sur leur machine locale, dans des environnements de test, ou sur des serveurs de production sans avoir à se soucier des différences d'environnement.

=== Envirionnement final
Le projet utilise un environnement et des logiciels qui sont disponibles aussi bien sur Windows, Mac et Linux. Nous avons utilisé :
- Mac v14.1.1
- Linux v6.6.7-4-MANJARO
- Fritzing v1.0.1
- Diagrams.net / drawio v22.1.16
- Typst v0.8 beta
- VSCode v1.85.1

= Résultats

Au terme de ce projet, nous avons abouti à la création de 13 livrables comprenant une note de clarification, un rapport de projet/passation, 7 tutoriels, et 4 portages d'anciens rapports rédigés par des étudiants précédents, réécrits en Typst. Notre objectif principal a été de développer une architecture de documentation incrémentable, facile à utiliser, et facilement portable sur n'importe quelle machine. Cette approche vise à simplifier la gestion et l'évolution de la documentation électronique au fil du temps.

Il est important de noter que, en raison de contraintes matérielles, seuls quelques tutoriels ont été réellement testés, et les photos de prototypage associées à ces réalisations sont limitées. Cependant, la conception et la mise en place d'une architecture robuste jettent les bases d'une documentation évolutive, prête à être complétée et testée au fur et à mesure que les ressources matérielles deviennent disponibles. Ces résultats marquent une étape significative vers la création d'une documentation électronique complète et accessible pour les étudiants travaillant sur des projets électroniques au sein de la plateforme de prototypage du Centre d'innovation.

#figure(
  image("images/tuto_arduino_new.png", width: 70%),
  caption: [Extrait du tutoriel arduino retravaillé],
)

== Liste des Tutoriels Réalisés

- ESP32 
  - Comment utiliser le PWM
- Électronique Analogique
  - Créez une Alimentation Réglable
  - Guide sur l'Utilisation des Relais
- Arduino
  - LED
  - Comment faire un système esclave-maître en I2C
  - Pourquoi utiliser des broches en mode pull-up et pull-down
  - Ecran OLED
  - Programmation du module CAN MCP2515
  - Comment contrôler un moteur à courant continu
- Raspberry
  - Surveillance de la Bande Passante

= Conclusion
== Cahier des charges
En rétrospective du cahier des charges initial, des ajustements ont été apportés à la liste des tutoriels. Certains ont été retirés, d'autres ajoutés, résultant en une sélection finale différente de celle initialement envisagée. Ces modifications ont été motivées par des considérations de pertinence dans le contexte immédiat ainsi que par des contraintes liées aux ressources disponibles. Le délai pour l'obtention du matériel a également impacté notre capacité à valider tous les tutoriels prévus initialement.

Pour la reprise en main de ce projet, nous conseillons d'avoir des bases en informatique (minimum GI01) pour gérer le projet dans son entièreté et être capable de le faire évoluer. Cependant, pour un usage plus restreint, comme la rédaction de tutoriels, n'importe quel étudiant peut s'en sortir en utilisant les outils web Gitlab UTC et en lisant le readme du projet. 

== Planification du projet
La planification du projet s'est basée sur un diagramme de Gantt pour assurer une gestion temporelle optimale. Nous avons maintenu les quatre réunions prévues avec notre responsable de PR, assurant ainsi une communication constante et des ajustements en temps réel selon les besoins du projet.

En outre, comme nous étions en apprentissage, nous avons dû alterner un mois à l'UTC puis un mois en entreprise et enfin un dernier mois à l'UTC. Par conséquent, nous avons réalisé plusieurs tâches en avance afin de ne pas être retardés par notre travail en entreprise.

== Reprise en main du projet
Pour continuer à développer le projet, les étudiants peuvent retrouver tous les fichiers sources sur le Gitlab UTC https://gitlab.utc.fr/khimayan/pr-redaction.git et un tutoriel détaillé d'installation dans le readme.pdf du projet. 

== Évolutions
Ce projet a été réalisé pendant le semestre A23 et nous avons identifié plusieurs pistes d'amélioration :

Continuer à retravailler les anciens tutoriels existants dans une volonté d'homogénéiser les tutoriels et de mettre à disposition sur GitLab les fichiers sources pour une amélioration continue.
Tester sur une plus grande échelle les tutoriels pour les améliorer davantage en prenant en compte les retours des utilisateurs avec une expérience différente en embarqué.

=== Propositions de Nouveaux Tutoriels

Nous suggérons d'ajouter les tutoriels suivants à la plateforme de documentation électronique :

- Raspberry Pi: Communication SSH sur les différents systèmes d'exploitation
  - Exploration de l'accès distant à la Raspberry Pi sur divers systèmes d'exploitation avec un accent particulier sur l'utilisation de PuTTY pour les utilisateurs de Windows.

- Raspberry Pi: Programmer des tâches avec le logiciel CRON
  - Guide sur l'automatisation des tâches avec le logiciel CRON sur la Raspberry Pi, offrant une approche pratique pour la planification de processus périodiques.


- Raspberry Pi: Utiliser une Raspberry comme routeur avec RaspAP
  - Exploration des configurations réseau pour utiliser une Raspberry Pi comme routeur avec RaspAP, ouvrant des opportunités pour des projets liés aux réseaux sans fil.

- Arduino: Compilation Bare-Metal avec la toolchain AVR-GCC
  - Guide avancé sur la compilation Bare-Metal avec la toolchain AVR-GCC pour Arduino, permettant aux utilisateurs d'explorer la programmation bas niveau sur les cartes Arduino.

- Arduino: Envoyer/Recevoir des fichiers à travers l'interface USB (Port Série)
  - Tutoriel sur l'échange de fichiers entre un ordinateur et Arduino via l'interface USB (Port Série), offrant des compétences pratiques en communication de données.

- Gestion d'un répertoire Gitlab UTC pour comprendre le versionning de fichier, l'intégration continue et les artefacts.

== Conclusion personnelle
=== Yaniss Khima
La réalisation de ce projet a été une expérience enrichissante pour moi. J'ai eu l'opportunité d'approfondir mes connaissances acquises lors de mon cursus en DUT GEII. La mise en œuvre d'une production continue avec GitLab CI/CD a été un aspect particulièrement instructif, me permettant de comprendre de manière concrète comment automatiser et améliorer le processus de développement.

L'ensemble du projet m'a apporté une satisfaction personnelle, en constatant la concrétisation de notre travail à travers la création d'une documentation électronique organisée et efficace. Ces compétences nouvellement acquises, combinées à la satisfaction du projet, renforcent mon engagement envers le domaine de l'électronique et du développement continu.

Ce projet a été une étape significative dans mon parcours académique et professionnel, et je suis reconnaissant pour les opportunités d'apprentissage qu'il a offertes.

=== Cécile Gaulter
Étant moi-même néophyte dans ce domaine, j'ai beaucoup aimé cette approche pédagogique et pratique qui m'a permis de concrétiser des concepts généraux en embarqué. Ce projet m'a apporté des connaissances complémentaires à ma formation à l'UTC. En outre, en échangeant avec Yaniss Khima qui était plus expérimenté, j'ai pu en apprendre plus sur les évolutions des outils mis à notre disposition (exemple : Pi5), améliorant ainsi ma culture générale en informatique.

J'ai également découvert de nouveaux outils de mise en forme comme Typst et Fritzing. Étant gratuits, assez intuitifs et faciles à prendre en main, je pourrais les utiliser dans de futurs projets informatiques.

J'ai pu évaluer ma progression lorsqu'en IoT, j'ai pu expliquer à d'autres étudiants néophytes le fonctionnement d'une Arduino. J'étais heureuse et fière lorsque j'ai vu les conséquences directes du travail fourni.


= Note de clarification

La plateforme de prototypage du Centre d’innovation est équipée de nombreux outils pour la réalisation de systèmes électroniques. Les élèves rencontrent fréquemment des questions récurrentes lors de leurs projets électroniques.

== Données d'Entrées
Liste des tutoriels sur Fablab
- https://fablabutc.fr/documentation-tutoriels-electroniques

== Objet du Projet
=== Phase de Réalisation
- Rédaction de tutoriels
- Création de schémas et photos d’expériences pratiques

=== Produit du Projet
- 8 tutoriels en format PDF
- Dépôt Git contenant les fichiers sources réalisés avec Typst
- Rapport de passation
- Soutenance de 30 minutes

== Objectifs Visés
- Qualité : Rédaction qualitative, approfondie, et complète des problématiques électroniques récurrentes
- Délai : Du 1er septembre au 12 janvier

== Acteurs
- Maître d’Ouvrage : Nicolas PITON
- Maîtres d'Œuvre : Cécile Gaultier et Yaniss Khima

== Conséquences Attendues
- Amélioration de la compréhension générale des élèves sur des sujets classiques d’électronique
- Capitalisation des connaissances et des recherches effectuées
- Amélioration de l'attractivité du fablab UTC

== Contraintes
- Contraintes de Délais : Rendu des tutoriels avant le 1er janvier, planning d’alternance du 16 octobre au 18 novembre et du 25 au 28 décembre
- Autres Contraintes : Proposer un travail en complémentarité et en cohérence avec les précédents tutoriels

== Planification Prévisionnel

#image("images/gantt.svg")