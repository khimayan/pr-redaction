#import "../../template.typ": *
#import "@preview/bytefield:0.0.1": *

#show: template.with(
  uv_name: "Arduino",
  title_name: "LED",
  authors: ("Benoit THOMAS",)
)

#par(first-line-indent: 0pt)[

/ Temps: 5 minutes (lecture et réalisation)

/ Difficulté: Facile

/ Matériel requis:
]

- Arduino UNO
- résistance
- LED

= Introduction

Une led est un petit composant lumineux utilisé en électronique et qui est très utile dans des montages Arduino pour voir une information, ou l’état d’une variable. Une diode électroluminescente (LED) est un dipôle qui émet de la lumière quand il est parcouru par un
courant.

= Objectif

Nous allons écrire un code pour commander l’allumage de la LED à partir de l’arduino. Pour
notre exemple l’objectif est d’allumer la LED 5 secondes puis de l’éteindre 2 secondes, en
boucle.

= Réalisation

La commande d’une LED est similaire à l’envoie d’un signal haut (5V).
Voici un exemple de code, très proche du code "Blink" 

#grid(columns: (1fr, 1fr),
  image("images/led_bb.svg", width: 80%),
  code[
```cpp
// Définit la broche à laquelle la LED est connectée
const int LED_PIN = 2;

void setup() {
  // Configure la broche de la LED en tant que sortie
  pinMode(LED_PIN, OUTPUT);
}

void loop() {
  // Allume la LED
  digitalWrite(LED_PIN, HIGH);
  // Attend 1 seconde
  delay(5000);
  digitalWrite(LED_PIN, LOW);
  delay(2000);
}

```
  ]
)

Ainsi, après avoir chargé le code, l’arduino ferme l’interrupteur 5 secondes, puis l’ouvre 2
secondes, en boucle.

= Ressources

- U=RI, Comment allumer une LED?

  https://youtu.be/JDlIkjVCKZE
