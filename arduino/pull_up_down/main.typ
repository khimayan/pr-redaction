#import "../../template.typ": *

#show: template.with(
  uv_name: "Arduino",
  title_name: "Pourquoi utiliser des broches en mode pull-up et pull-down?",
)

#par(first-line-indent: 0pt)[

/ Temps: 10 minutes (lecture et réalisation)

/ Difficulté: facile

/ Matériel requis: Arduino UNO; Boutton poussoir
]

= Introduction
Imaginez que vous devez utiliser un bouton poussoir qui, lorsqu'il est en position initiale, rompt le circuit, rendant ainsi impossible de déterminer la tension dans le système. Afin de remédier à cette situation, il est nécessaire d'assurer un état prévisible pour une broche lorsque aucun signal externe n'est actif. Vous pouvez accomplir cela en utilisant les configurations de résistance de tirage haut (pull-up) ou de tirage bas (pull-down) pour les broches correspondantes.

= Les Modes "Pull-Up" et "Pull-Down" (Résistances de Tirage)

Les modes "pull-up" et "pull-down", également appelés "résistances de tirage", sont des configurations couramment utilisées dans les systèmes électroniques pour fixer la tension d'une broche quel que soit l'état du circuit. Ces modes sont particulièrement utiles pour atténuer les perturbations et les variations dans la source d'énergie, assurant ainsi un signal stable.

== Mode Pull-Up

Lorsque vous configurez une broche Arduino en mode "pull-up", vous tirez le niveau électrique vers le haut en définissant la valeur de la broche à `HIGH`. Cette configuration est couramment utilisée pour les entrées (INPUT) car elle connecte la broche à la tension d'alimentation, pouvant atteindre jusqu'à 5V selon la carte électronique utilisée.

Exemple d'utilisation :

#code()[
```cpp
pinMode(pinBouton, INPUT_PULLUP); // Configure la broche en mode pull-up
```
]

== Mode Pull-Down

Le mode "pull-down" permet d'abaisser le niveau électrique en définissant la valeur de la broche à `LOW`. Dans ce cas, la broche est également en mode entrée (INPUT), mais elle est connectée à la masse (0V) via la résistance de tirage.

Exemple d'utilisation :

#code()[
```cpp
pinMode(pinInterrupteur, INPUT_PULLDOWN); // Configure la broche en mode pull-down
```
]


#info()[Pour illustrer ces modes, prenons l'exemple d'un bouton-poussoir. Vous pouvez connecter ce bouton entre une broche et la masse. En activant une résistance de pull-up interne, la broche reste à l'état HIGH tant que le bouton n'est pas enfoncé. Lorsque le bouton est enfoncé, la broche est tirée à la masse et passe à LOW.]

= Documentation
== Fonction pinMode()

La fonction `pinMode()` est une fonction essentielle en programmation Arduino qui permet de définir le mode de fonctionnement d'une broche (pin) spécifique sur la carte Arduino. Cette fonction prend deux paramètres : `pin` et `mode`.

==== Paramètres

- `pin` (Numéro de la broche Arduino):
  - Type de données : Entier (int)
  - Description : Le paramètre `pin` spécifie le numéro de la broche sur laquelle vous souhaitez définir le mode de fonctionnement. Vous devez spécifier le numéro de broche en utilisant la numérotation de la carte Arduino, par exemple, 2 pour la broche 2.

- `mode` (Mode de la broche):
  - Type de données : Chaîne de caractères (String)
  - Description : Le paramètre `mode` détermine le mode de fonctionnement de la broche spécifiée. Vous avez trois options :
    - `INPUT` : Cette option configure la broche en tant qu'entrée (input), ce qui signifie qu'elle peut lire des signaux provenant de capteurs ou d'autres périphériques.
    - `OUTPUT` : Cette option configure la broche en tant que sortie (output), ce qui signifie qu'elle peut envoyer des signaux électriques vers d'autres composants ou dispositifs.
    - `INPUT_PULLUP` : Cette option configure la broche en tant qu'entrée avec résistance de pull-up interne activée. Elle est souvent utilisée pour les boutons-poussoirs et les interrupteurs.

= Exemple d'utilisation

pour configurer une broche en mode `INPUT_PULLUP`, ce qui active la résistance de pull-up interne :

#grid(columns: (1fr, 1fr),
  
  figure()[
    #image("./images/Arduino_button.svg", height: 35%)
  ],

  [
    
    #set text(size: 9.5pt)

    #code()[
      ```cpp
      // Définit la broche 2 comme broche du bouton.
      int buttonPin = 2;

      void setup() {
        // Configure la broche en mode INPUT_PULLUP.
        pinMode(buttonPin, INPUT_PULLUP);
      }

      void loop() {
        // Lisez l'état du bouton
        //(HIGH lorsqu'il est relâché, LOW lorsqu'il est enfoncé).
        int buttonState = digitalRead(buttonPin);

        if (buttonState == LOW) {
          // Le bouton est enfoncé.
          // Effectuez ici les actions nécessaires.
        }
      }
      ```
    ]
  ],
)

Dans cet exemple, la broche 2 est configurée en mode `INPUT_PULLUP` à l'aide de `pinMode()`. Cela permet de lire l'état d'un bouton connecté à cette broche, où la résistance de pull-up interne est activée pour simplifier la détection des pressions de bouton.


== Conclusion

#avantages[
=== Économie d'Énergie
Le système permet de maintenir en permanence une broche à un niveau actif avec une source de courant externe.

=== Détection de changements d'état
Le passage de HIGH à LOW (ou inversement) est facilement détecté lorsque le bouton est actionné.

=== Simplicité de conception
Les deux modes sont directement intégrés dans une carte Arduino. Le système n'a pas besoin d'ajouter des composants externes plus complexes pour obtenir le même résultat.
]

#inconvenients[
=== Temps de montée et de descente
Les temps de montée/descente sont plus lents pour les signaux car la résistance des broches augmente le temps nécessaire pour décharger la ligne de signal à un niveau logique bas.

=== Sensibilité au bruit 
Le mode pull-down peut être plus sensible au bruit électromagnétique et aux interférences, car il connecte la ligne de signal à la masse. Cela peut rendre les signaux plus sujets aux erreurs de lecture.
]

= References
- https://www.arduino.cc/reference/en/language/functions/digital-io/pinmode/
- https://docs.arduino.cc/tutorials/generic/digital-input-pullup 
- https://www.wikidebrouillard.org/wiki/Comprendre_les_r%C3%A9sistances_de_pull-up_et_pull-down
- https://www.seeedstudio.com/blog/2020/02/21/pull-up-resistor-vs-pull-down-differences-arduino-guide/ 
