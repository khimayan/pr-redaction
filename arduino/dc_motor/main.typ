#import "../../template.typ": *
#import "@preview/bytefield:0.0.1": *

#show: template.with(
  uv_name: "Arduino",
  title_name: "Comment contrôler un moteur à courant continu ?",
  authors: ("Kilian Sanfins",)
)

#par(first-line-indent: 0pt)[

/ Temps: 30 minutes (lecture et réalisation)

/ Difficulté: Difficile
]

= Introduction

Le moteur à courant continu est un moteur très simple d'utilisation et le moins cher. Pour son fonctionnement il suffit de lui appliquer une tension pour sa mise en rotation. Plus la tension appliquée est élevée, plus le moteur tournera vite et plus le courant sera fort, plus il aura de couple (attention tout de même à ne pas dépasser les valeurs limites fournies par le constructeur!).

Cependant le courant requis peut être trop élevépour qu'il soit fourni directement par le microcontrolleur. On va donc utiliser un dispositif appelé contrôleur (driver en anglais) tel que le L298 pour le faire tourner à pleine
puissance.

#avantages[
- Peu coûteux
- Facile d’utilisation
- Pour les moteurs les plus petits il est possible de les contrôler directement avec le microcontrôleur.
- Contrôle en vitesse aisé.
]

#inconvenients[
- Le contrôle en position nécessite un encodeur et l’asservissement du moteur
- C’est une technologie peu compacte (ratio puissance/volume/poids faible)
- Un moteur à courant continu tourne vite, il nécessite une boîte de réduction pour obtenir un couple décent en sortie (ou alors il sera sur dimensionné pour produire un gros couple à faible vitesse)
- A cause des balais, ce n’est pas le moteur le plus silencieux
]

#info(title: "Cas d'utilisation")[
- Mettre un système en mouvement à bas coût et avec simplicité
- Où l’on souhaite une certaine vitesse de rotation, même si avec un bon asservissement, il est possible de le contrôler en couple ou en position aisément. 
]

= Schéma électrique

#image("images/l298n-arduino_bb.svg")

#pagebreak()

= Code

#code[
```cpp
// Pin de commande du moteur L298
const int IN1 = 9;
const int IN2 = 8;

void setup() {
  pinMode(IN1, OUTPUT); // Configure la pin IN1 en sortie
  pinMode(IN2, OUTPUT); // Configure la pin IN2 en sortie
  ArreterMoteur(); // Arrête le moteur au démarrage du programme
}

void loop() {
  // Tourne le moteur dans un sens à vitesse maximale
  TournerMoteur(225, 0);
  delay(1000);
  ArreterMoteur();
  delay(1000);
  // Tourne le moteur dans l'autre sens à la moitié de la vitesse
  TournerMoteur(125, 1);
  delay(1000);

/*
 * Fonction pour faire tourner le moteur L298.
 * Paramètres :
 *   - vitesse : La vitesse de rotation du moteur (0 à 255)
 *   - sens : true pour le sens horaire, false pour le sens anti-horaire
 */
void TournerMoteur(int vitesse, bool sens) {
  // Limite la valeur de vitesse entre 0 et 255
  vitesse = constrain(vitesse, 0, 255);

  // Active le bras du pont en H en fonction du sens de rotation souhaité
  if (sens) {
    digitalWrite(IN2, LOW);
    analogWrite(IN1, vitesse);
  } else {
    digitalWrite(IN1, LOW);
    analogWrite(IN2, vitesse);
  }
}

/*
 * Fonction pour arrêter la rotation du moteur L298.
 */
void ArreterMoteur() {
  // Coupe l'alimentation des deux bras du pont en H
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
}
```
]


= Explication
Le contrôleur L298 est un pont en H. Ce nom lui vient de la forme de son schéma électrique.

#figure(caption: [https://fr.wikipedia.org/wiki/Pont_en_H])[
  #image("images/Pont_En_H.png", width: 60%),
]


En fonction des interrupteurs activés (nommé S1, S2, S3, S4), il est possible d'alimenter une charge (ici un Moteur) dans un sens ou dans l'autre. Certaines combinaisons sont interdites.
Voici une liste des connections les plus importantes:

#align(center)[
  #table(columns: 5, rows: 7,
    [S1], [S2], [S3], [S4], [Résultat],
    [✓], [✗], [✗], [✓], [Le moteur tourne dans le sens 1],
    [✗], [✓], [✓], [✗], [Le moteur tourne dans le sens 2],
    [✗], [✗], [✗], [✗], [Le moteur ne tourne pas],
    [✗], [✓], [✗], [✓], [Court-circuit - *NE PAS FAIRE*],
    [✓], [✗], [✓], [✗], [Court-circuit - *NE PAS FAIRE*],
    [✓], [✓], [✓], [✓], [Court-circuit - *NE PAS FAIRE*],
  )
]
#par(first-line-indent: 0pt)[
  / ✓ : interrupteur fermé (laisse passer le courant)
  / ✗ :  interrupteur ouvert (ne laisse pas passer le courant)
]

Il est possible de fabriquer son propre pont en H avec des transistors mais pour simplifier la réalisation, nous avons choisi un composant qui gère tout et simplifie le contrôle du moteur. En particulier, le moteur se contrôle avec 2 pins, l'un pour faire tourner le moteur dans un sens et l'autre pin pour l'autre sens (le sens de rotation dépend du branchement du moteur).

#info(title: "Attention")[
Si les 2 pins sont activés en même temps, il y a court-circuit!
]

Le code sert précisément à cela, il désactive une pin pour activer l'autre et ainsi contrôler le moteur! Les fonctions fournies dans le code permettent un contrôle facile et compréhensible du moteur.

En activant et en coupant rapidement les bras de pont le L298 produit de la Modulation de Largeur d’Impulsion (abrégé MLI ou PWM en anglais pour Pulse Width Modulation). Sans rentrer dans les détails, pour chaque période, le signal est à l’état haut pendant une certaine durée t et à

l’état bas le reste du temps. On définit alors le ratio: $"Temps à l état haut" / "Durée d'une période"$ comme le rapport cyclique (duty cycle en anglais).
#align(center)[
  #image("images/004.png", width: 70%)
]

Ainsi, un rapport cyclique de 10 % voudrait dire que 10% du temps, le signal est à l’état haut, le reste à l’état bas. Dans le cas du contrôle du moteur, l’état haut correspond à la tension maximum fournie par l’alimentation. L’état bas est la masse.

Donc on applique un signal PWM au moteur avec un rapport cyclique de 10%, le moteur ne verra que 10% de la tension maximum. Ce principe là est utilisé pour faire varier la vitesse du moteur grâce au L298.

Cependant le moteur n’est pas protégé de la tension fournie par l’alimentation, si le moteur ne supporte que 12v et qu’une tension de 24v est appliquée au L298, il est possible de le faire tourner en envoyant une faible commande (rapport cyclique bas, la tension ressentie par le moteur sera alors inférieure à 12v) mais si l’on envoie la commande maximum (rapport cyclique maximum donc 100%, ce qui correspond dans notre cas à 24v) alors le moteur sera détruit!

#pagebreak()
= Ressources

- https://howtomechatronics.com/tutorials/arduino/arduino-dc-motor-control-tutorial-l298n-pwm-h-bridge/

- https://wiki.mchobby.be/index.php?title=Pont-H_L298N
  
- https://www.sparkfun.com/datasheets/Robotics/L298_H_Bridge.pdf
