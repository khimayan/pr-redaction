#import "../../template.typ": *
#import "@preview/bytefield:0.0.1": *

#show: template.with(
  uv_name: "Arduino",
  title_name: "Comment faire un système esclave/maître en I2C ?",
)

#par(first-line-indent: 0pt)[

/ Temps: 30 minutes (lecture et réalisation)

/ Difficulté: Difficile

/ Matériel requis:
]

- Arduino UNO
- résistances de tirage
- MPU6050


= Introduction

Le bus I2C (Inter-Integrated Circuit) est un protocole de communication en série, utilisé pour connecter divers composants électroniques. Ce tutoriel vous guidera pour établir une communication entre un maître et un esclave sur un bus I2C pour trouver l'adresse I2C d'un capteur.

= Topologie du Réseau I2C

Le bus I2C est un bus de communication série qui suit une structure hiérarchique, où tous les dispositifs sont connectés au même bus de données. Chacun des dispositifs sur le bus possède une adresse unique qui lui permet d'être distingué par le maître lors des communications. Cette architecture en série signifie que tous les dispositifs partagent les mêmes lignes de données et d'horloge, créant ainsi un réseau interconnecté.

#info()[
== Maître 
Les maîtres sont les composants qui contrôlent la communication sur le bus I2C. Ils initient les transactions, envoient des commandes et dirigent la séquence de communication. En général, un réseau I2C aura un ou plusieurs maîtres, bien que seulement un soit actif à un moment donné.

== Esclave
Les esclaves sont les dispositifs qui répondent aux commandes des maîtres. Ils sont passifs jusqu'à ce qu'ils soient sollicités par un maître pour effectuer une opération particulière. Les esclaves peuvent être des capteurs, des mémoires, des actionneurs ou d'autres périphériques qui fournissent des données ou effectuent des actions en réponse aux requêtes du maître.
]

La distinction entre maîtres et esclaves est essentielle pour la gestion de la communication sur le bus I2C, car elle définit les rôles et les responsabilités de chaque dispositif dans le réseau.

== Câblage du bus I2C

=== SDA (Serial Data)
La ligne SDA transporte les données entre le maître et les esclaves. Les informations sont transmises en série sur cette ligne.

=== SCL (Serial Clock)
La ligne SCL transporte le signal d'horloge qui synchronise la transmission des données entre les dispositifs. Tous les dispositifs sur le bus suivent ce signal pour assurer une communication coordonnée.

En plus des lignes SDA et SCL, il est essentiel d'ajouter des résistances de tirage sur ces lignes pour garantir un niveau logique haut stable lorsque les lignes ne sont pas actives. Ces résistances sont généralement connectées entre les lignes SDA et SCL et l'alimentation (VCC).

= Structure d'une Trame I2C

#let RW = [R/#overline([W])]

=== Définition 
La structure d'une trame I2C est composée de deux lignes de communication (SDA et SCL), ainsi que de plusieurs bits d'information :
#info()[
- *`Start Condition`* : signal de départ (START) émis par le maître pour indiquer le début de la transmission. La ligne SDA passe de l'état (1) à (0) pendant que la ligne SCL est à (1)
- *`Adresse de l'esclave`* : le maître envoie l'adresse du périphérique avec lequel il souhaite communiquer grâce à son adresse unique
- *`Acknowledgment Bit`* ("accusé de réception") : l'esclave envoie un bit d'accusé de réception (ACK) pour indiquer s'il est prêt à communiquer ou non (NACK ou #overline()[ACK])
- *`Data`* : si le maître reçoit un ACK, alors il envoie à l'esclave (ou vice versa) des octets de données (commandes, valeurs de capteurs, configurations, etc.)
- *`Acknowledgment Bit for Data`* : Après chaque octet de données, un ACK ou #overline()[ACK] est émis par le récepteur pour indiquer si les données ont été correctement reçues
- *`Stop condition`* : le maître génère un signal d'arrêt (STOP) pour indiquer la fin de la trame
]
=== Exemple de lecture / écriture  
Le maître envoie une trame qui commence par le bit `Start` afin d'inqiquer le début de la structure. L'adresse d'un esclave est composée de 7 bits et le 8ème indique si le maître souhaite lire ou écrire des données. Si le 8e bit est à 0, alors il veut écrire, sinon il est à 1 pour lire les données (#RW (Read/Write)).

Ensuite, l'esclace indique s'il est disponible ou non grâce au `Acknowledgment Bits`. Si les deux sont libres, alors ils peuvent s'échanger des données jusqu'à la fin avec le bit stop. 

#bytefield(
  bits: 9,
  
  bits(7)[Adresse de l'Esclave (7b)], bits(1)[#RW (1b)], bits(1)[ACK (1b)],
  
  bits(8)[Adresse mémoire de l'Esclave (Optionnel)], bits(1)[ACK],

  bits(8)[Données], bits(1)[ACK],

  bits(9)[...],

  bits(8)[Données], bits(1)[#overline()[ACK]],
)

= Adresses I2C
Chaque périphérique sur le bus I2C possède une adresse unique sur 7 Bits. Certaines adresses sont réservées pour diffuser des messages, tandis que d'autres sont attribuées par les fabricants. Voici quelques adresses importantes :

#table(columns: (auto, auto), 
  [*Adresse*], [*Fonction*],
  [0000 0110], [RESET],
  [0000 0010], [Rechargement logiciel d'adresse esclaves],
  [0000 0100], [Rechargement matériel d'adresse esclaves],
)


= Exemple Pratique avec Arduino
L'IDE Arduino intègre nativement la gestion du bus I2C via la librairie Wire.h. Voici un exemple simple pour établir une communication I2C entre un maître (Arduino) et un esclave (un autre périphérique compatible I2C) :

#text(size: 9.5pt)[
#code()[
```cpp
#include <Wire.h>
const int MPU6050_ADDR = 0x68; // Adresse I2C du MPU-6050

int16_t accelerometerX, accelerometerY, accelerometerZ;
int16_t gyroscopeX, gyroscopeY, gyroscopeZ;

void setup() {
  // Initialise le bus I2C
  Wire.begin();
  Serial.begin(9600);
  
  // Initialisation du MPU-6050
  Wire.beginTransmission(MPU6050_ADDR);
  Wire.write(0x6B); // Registre de gestion de l'alimentation
  Wire.write(0);    // Réveille le MPU-6050 (0 = Actif)
  Wire.endTransmission();
}

void loop() {
  // Lecture des données de l'accéléromètre et du gyroscope
  Wire.beginTransmission(MPU6050_ADDR);
  Wire.write(0x3B); // Registre de début des données
  // Envoi d'un signal de reprise pour garder la connexion active
  Wire.endTransmission(false);
  Wire.requestFrom(MPU6050_ADDR, 14); // Lecture de 14 octets de données

  accelerometerX = Wire.read() << 8 | Wire.read();
  accelerometerY = Wire.read() << 8 | Wire.read();
  accelerometerZ = Wire.read() << 8 | Wire.read();
  
  gyroscopeX = Wire.read() << 8 | Wire.read();
  gyroscopeY = Wire.read() << 8 | Wire.read();
  gyroscopeZ = Wire.read() << 8 | Wire.read();

  // Affichage des données
  Serial.print("Accéléromètre (X,Y,Z) : ");
  Serial.print(accelerometerX);
  Serial.print(", ");
  Serial.print(accelerometerY);
  Serial.print(", ");
  Serial.println(accelerometerZ);

  Serial.print("Gyroscope (X,Y,Z) : ");
  Serial.print(gyroscopeX);
  Serial.print(", ");
  Serial.print(gyroscopeY);
  Serial.print(", ");
  Serial.println(gyroscopeZ);
  
  delay(1000); // Pause d'une seconde
}
```
]
]

Dans cet exemple, nous utilisons la bibliothèque Wire.h pour communiquer avec le MPU-6050 via le bus I2C. Nous initialisons le MPU-6050 et lisons les données de l'accéléromètre et du gyroscope à partir de ses registres.

Vous pouvez afficher les données de l'accéléromètre et du gyroscope dans le moniteur série Arduino pour surveiller les lectures en temps réel.

= Conclusion

Le bus I2C est un moyen efficace de connecter plusieurs composants électroniques sur un même réseau. En comprenant la topologie du réseau, les adresses I2C, les modes d'échange et en utilisant les bibliothèques appropriées, vous pouvez établir une communication I2C fiable entre un maître et un esclave pour diverses applications.

= Bibliographie
- https://les-electroniciens.com/sites/default/files/cours/cours_i2c.pdf

- https://f-leb.developpez.com/tutoriels/arduino/bus-i2c/

- https://invensense.tdk.com/wp-content/uploads/2015/02/MPU-6000-Datasheet1.pdf

- https://invensense.tdk.com/wp-content/uploads/2015/02/MPU-6000-Register-Map1.pdf