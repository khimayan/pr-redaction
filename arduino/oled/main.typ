#import "../../template.typ": *
#import "@preview/bytefield:0.0.1": *

#show: template.with(
  uv_name: "Arduino",
  title_name: "Ecran OLED",
  authors: ("Benoit THOMAS", )
)

#par(first-line-indent: 0pt)[

/ Temps: 20 minutes (lecture et réalisation)

/ Difficulté: Moyen

/ Matériel requis:
]

- Arduino UNO
- Écran OLED

= Introduction

L’écran OLED permet d’afficher tous types d’information, images ou textes. Il existe des écrans OLED adaptés pour une utilisation avec un arduino. Nous allons voir dans ce tutoriel comment les utiliser. A travers ce tutoriel l’objectif est de savoir utiliser les écrans OLED pour afficher du texte ou un logo.

Afin de comprendre le fonctionnement des écrans OLED nous allons étudier 2 exemples.
Dans le premier exemple nous allons voir comment afficher du texte, et ensuite nous verrons comment afficher une image/logo.

Pour ce tutoriel, il faut installer plusieurs librairies : selon votre écran soit la librairie `Adafruit_SSD1306.h`, soit Adafruit_SH1106.h. Egalement la librairie `Adafruit_GFX.h` sera nécessaire dans les deux cas.

= Objectif
Nous allons écrire un code pour afficher ce que l’on souhaite, puis nous allons envoyer le
code sur l’arduino. Enfin l’arduino va communiquer par 2 pins pour afficher l’information
souhaité sur l’écran.

= Schéma et câblage
Pour commencer, nous allons câbler comme ci-dessous :
#align(center)[
  #image("images/OLED_bb.png", width: 50%)
]

= Réalisation
== Afficher du texte

Pour afficher du texte c’est relativement simple, il faut utiliser la commande :

```cpp display.print("texte à afficher");```

Cependant il est nécessaire de configurer l’écran avant de pouvoir afficher le texte. Pour cela il faut mettre toutes les commandes expliquées ci-dessous :

#par(first-line-indent: 0pt)[
/ ```cpp display.clearDisplay();```: Pour vider l’écran

/ ```cpp display.setTextSize(9);```: Choisir la taille du texte (ici 9 Pixels)
/ ```cpp display.setTextColor(WHITE);```: Choisir la couleur du texte
/ ```cpp display.setCursor(0,0);```: Mettre le curseur en haut à gauche
/ ```cpp display.print(i);```: Afficher la valeur de i
/ ```cpp display.display();```: Mettre à jour l’écran et afficher le texte
]


Pour mettre en application l’affichage de texte nous allons faire un exemple très simple : afficher un chronomètre de 60 secondes.

Pour cela il suffit de créer une boucle for et à chaque seconde changer l’affichage de n+1. Voici le code (fichier : `aff_texte`):

#code[
```cpp
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH1106.h>

#define OLED_RESET 4

Adafruit_SH1106 display(OLED_RESET);

void setup() {
  Serial.begin(9600);
  display.begin(SH1106_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();
}

void loop() {
  display.clearDisplay();
  display.setTextSize(9);
  display.setTextColor(WHITE);

  for (int i = 1; i < 60; i++) {
    display.setCursor(0, 0);
    display.print(i);
    display.display();
    delay(1000);
    display.clearDisplay();
    display.display();
    delay(1000);
  }
}
```
]

== Afficher un logo

Pour afficher une image ou un logo, il faut convertir l’image en un code compréhensible pour l’arduino afin qu’il affiche l’image.

Pour cela nous allons utiliser le site suivant : https://diyusthad.com/image2cpp qui permet de convertir une image en format Arduino.

#image("images/005.png", width: 60%)

Il faut ensuite mettre l’image avec une définition de 128 $times$ 64 pixels. C’est la définition "classique" des écrans OLED, mais si votre écran est différent il faut mettre les dimensions de votre écran.

Les autres paramètres sont optionnels. Vous pouvez ensuite voir si le rendu vous satisfait.

#image("images/006.png")

Ensuite il faut configurer le format de sortie pour générer un code lisible par l’arduino. Il faut choisir « Arduino Code », puis cliquer sur générer.


#image("images/007.png")

Pour le code, il suffit de copier coller le code de l’image et de le placer dans le code arduino.

#code[
```cpp
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH1106.h>

#define OLED_RESET 4
Adafruit_SH1106 display(OLED_RESET);

const unsigned char logo16_glcd_bmp[] PROGMEM = {
  // Copier-Coller le code du logo ici
  // Remplissez cet espace avec les données de votre logo
};

void setup() {
  Serial.begin(9600);
  display.begin(SH1106_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();

  display.drawBitmap(1, 1, logo16_glcd_bmp, 128, 64, 1);
  display.display();
}

void loop() {
  // Code pour la boucle si nécessaire
}
```
]

On obtient alors :

#align(center)[
  #image("images/011.png", width: 50%)
]

= Pour aller plus loin

Il est possible de créer des fonctions afin d’afficher des animations. Par exemple dans le code d’exemple de la librairie, il a plusieurs fonctions qui permettent de créer des animations semblables à un écran de veille.

De plus il est également possible de créer un menu est de mettre en variable certaines informations, pour les faire varier en fonction du temps.

= References

- DIY USTHAD, Displaying Images in OLED Display
  
  https://diyusthad.com/2019/02/display-images-in-oled-display.html

- Crazy Couple, Tutorial on I2C OLED Display with Arduino/NodeMCU 

  https://youtu.be/_e_0HJY0uIo


- DIY USTHAD, Interfacing OLED 128x64 I2C with Arduino Uno

  https://youtu.be/9xZmWZ4Hh6M
