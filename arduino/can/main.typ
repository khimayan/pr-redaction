#import "../../template.typ": *
#import "@preview/bytefield:0.0.1": *

#show: template.with(
  uv_name: "Arduino",
  title_name: "Programmation du module CAN MCP2515",
)

#par(first-line-indent: 0pt)[

/ Temps: 15 minutes (lecture et réalisation)

/ Difficulté: Moyenne

/ Matériel requis:
]

- Arduino UNO
- MCP2515


= Introduction

Le MCP2515 est un contrôleur CAN (Controller Area Network) largement utilisé pour la communication série entre les microcontrôleurs. Il est couramment utilisé dans les applications automobiles et industrielles en raison de sa robustesse et de sa fiabilité. Ce tutoriel permet de se familiariser avec les bases de son utilisation sur un Arduino.

= Connexion matérielle

Connectez votre module CAN MCP2515 à votre Arduino comme sur le schéma suivant :

- VCC du MCP2515 à 5V de l'Arduino
- GND du MCP2515 à GND de l'Arduino
- SCK du MCP2515 à la broche 13 de l'Arduino
- SI du MCP2515 à la broche 12 l'Arduino
- SO du MCP2515 à la broche 11 l'Arduino
- CS du MCP2515 à la broche 10 de l'Arduino (Ceci peut être modifié dans le code)

#figure(
  image("./images/CAN.svg", width: 80%),
  caption: [Schéma Fritzing du module CAN à une Arduino],
)

= Installation de la bibliothèque CAN

#info()[Avant de pouvoir programmer le MCP2515, vous devez installer la bibliothèque "MCP_CAN" pour Arduino. Un tutoriel est disponible à cette adresse :
https://fablabutc.fr/wp-content/uploads/2021/01/Tutoriel_Installer-une-bibliotheque-pour-Arduino.pdf]


= Envoie du code exemple

Voici un exemple simple pour initialiser le MCP2515 et envoyer un message CAN. Copiez le code ci-dessous dans votre IDE Arduino et téléversez-le sur votre carte Arduino.

#code()[
  ```cpp
  #include <SPI.h>
  #include <mcp_can.h>

  MCP_CAN CAN0(10); // Utilisez le pin 10 comme CS (Chip Select)

  void setup() {
    Serial.begin(9600);
    while (CAN0.begin(MCP_ANY, CAN_500KBPS, MCP_8MHZ) != CAN_OK) {
      Serial.println("Erreur d'initialisation du MCP2515");
      delay(500);
    }
    Serial.println("MCP2515 initialisé avec succès!");
  }

  void loop() {
    unsigned char stmp[8] = {0, 1, 2, 3, 4, 5, 6, 7};
    CAN0.sendMsgBuf(0x123, 0, 8, stmp); // Envoyer un message CAN
    delay(1000);
  }
  ```
]

= Conclusion
C'est tout ! Vous avez maintenant programmé avec succès le module CAN MCP2515 avec Arduino. Vous pouvez maintenant personnaliser le code pour répondre à vos besoins spécifiques en matière de communication CAN.


#avantages[- Coût abordable.
- Facilement intégrable avec Arduino et d'autres microcontrôleurs.
- Utilisation courante dans l'industrie automobile et industrielle.
- Bonne documentation disponible en ligne.]

#inconvenients[- Limité à la communication CAN 2.0B à 1 Mbit/s.
- Nécessite une bibliothèque tierce pour simplifier la programmation.
- Peut nécessiter des ajustements matériels pour s'adapter à d'autres tensions CAN.]


= References 

- https://www.arduino.cc/reference/en/libraries/mcp_can/
- https://github.com/coryjfowler/MCP_CAN_lib