#import "../../template.typ": *

#show: template.with(
  uv_name: "ESP32",
  title_name: "Comment utiliser le PWM?",
)

#par(first-line-indent: 0pt)[

/ Temps: 10 minutes (lecture et réalisation)

/ Difficulté: Facile

/ Matériel requis:
]

- ESP32
- Résistances
- LEDs

= Introduction

La PWM (Modulation de Largeur d'Impulsion) est une technique couramment utilisée en électronique pour contrôler la vitesse d'un moteur, la luminosité d'une LED ou la tension d'un circuit, entre autres applications. Elle fonctionne en modulant rapidement une série de signaux d'impulsions pour ajuster la puissance fournie à un dispositif ou un composant.

#figure(caption: {
  [Schéma représentatif d'un signal PWM]
  footnote[http://www.hho4free.com/pulse_width_modulator__pwm.html]
  })[
#image("./images/duty_cycle.png")
]

L'ESP32 dispose de 16 canaux de PWM, ce qui signifie que vous pouvez contrôler jusqu'à 16 composants ou dispositifs différents en utilisant le PWM simultanément. Chaque canal PWM est capable de générer un signal PWM indépendant avec une résolution pouvant aller jusqu'à 16 bits.

= Initialisation du canal PWM

Pour utiliser un canal PWM, vous devez d'abord l'initialiser avec les paramètres souhaités tels que la fréquence et la résolution. Par exemple, voici comment initialiser le canal 0 avec une fréquence de 5000 Hz et une résolution de 8 bits (0-255) :

#code()[
  ```cpp
  const int ledChannel = 0;
  const int freq = 5000;
  const int resolution = 8;
  ledcSetup(ledChannel, freq, resolution);
  ```
]

= Attribution de la broche au canal PWM

Ensuite, vous devez attribuer le canal PWM à une broche spécifique de l'ESP32. Par exemple, si vous souhaitez contrôler une LED connectée à la broche GPIO 2 avec le canal 0, vous pouvez le faire comme suit :

#code()[
  ```cpp
  const int ledPin = 2;
  ledcAttachPin(ledPin, ledChannel);
  ```
]

= Écriture de la valeur du duty cycle

Pour contrôler la luminosité ou la vitesse d'un dispositif, vous pouvez utiliser la fonction `ledcWrite()` pour définir la valeur du devoir du signal PWM. Par exemple, pour régler la luminosité de la LED à 50 %, vous pouvez écrire une valeur de 127 (sur une résolution de 8 bits) :
#code()[
  ```cpp
  const int dutyCycle = 127;
  ledcWrite(ledChannel, dutyCycle);
  ```
]

= Connexion du Matériel

+ Branchez la LED sur la broche 2 de l'ESP32.
+ Connectez une résistance de 220 ohms entre la LED et la masse (GND) sur l'ESP32.

#align(center)[
  #image("./images/esp_pwm_bb.png", width: 50%)
]

= Programmation de l'ESP32

Voici le code Arduino pour contrôler la luminosité de la LED à l'aide du PWM avec la bibliothèque "LEDC" :

#code()[
  ```cpp
  #include <Arduino.h>
  #include <driver/ledc.h>  // Include the LEDC library for PWM


  const int ledPin = 2;  // GPIO2 pour la LED
  const int ledChannel = 0;  // Canal PWM 0
  const int pwmFrequency = 5000;  // Fréquence PWM en Hz
  const int pwmResolution = 8;  // Résolution PWM (de 0 à 255)

  void setup() {
    // Initialise le canal PWM avec la fréquence et la résolution spécifiées
    ledcSetup(ledChannel, pwmFrequency, pwmResolution);
    
    // Associe le canal PWM à la broche GPIO
    ledcAttachPin(ledPin, ledChannel);
  }

  void loop() {
    // Fait varier la luminosité de la LED en modifiant la valeur du devoir (duty cycle)
    for (int dutyCycle = 0; dutyCycle <= 255; dutyCycle++) {
      // Modifie le devoir du canal PWM
      ledcWrite(ledChannel, dutyCycle);
      
      // Attend un court laps de temps pour voir la variation
      delay(10);
    }
    
    // Fait varier la luminosité dans l'autre sens
    for (int dutyCycle = 255; dutyCycle >= 0; dutyCycle--) {
      ledcWrite(ledChannel, dutyCycle);
      delay(10);
    }
  }
  ```
]

= Conclusion

En résumé, l'ESP32 offre un puissant système de modulation de largeur d'impulsion (PWM) grâce à la bibliothèque "LEDC." Vous pouvez configurer et contrôler jusqu'à 16 canaux PWM indépendants pour contrôler la luminosité des LEDs, la vitesse des moteurs, ou d'autres dispositifs nécessitant une variation de puissance. Chaque canal peut être configuré avec une fréquence et une résolution spécifiques en fonction de vos besoins.

= Ressources

- http://www.hho4free.com/pulse_width_modulator__pwm.html

- https://espressif-docs.readthedocs-hosted.com/projects/arduino-esp32/en/latest/api/ledc.html