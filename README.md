# Projet de Rédaction de Tutoriels Électroniques

Ce projet est réalisé par Cécile GAULTIER et Yaniss KHIMA dans le cadre du Centre d'Innovation. L'objectif principal est de rédiger des tutoriels électroniques complets pour aider les élèves à résoudre des problématiques récurrentes dans leurs projets électroniques.

## Compilation des fichiers main.typ avec Typst

Pour compiler les fichiers main.typ avec Typst, suivez ces étapes simples :

1. Assurez-vous d'avoir [Typst](https://typst.app) installé sur votre système.

2. Clonez le dépôt Git contenant les fichiers main.typ :
   ```bash
   git clone https://gitlab.utc.fr/khimayan/pr-redaction.git
    ```

3. Accédez au répertoire contenant les fichiers main.typ que vous souhaitez compiler.

4. Ouvrez un terminal dans ce répertoire.

5. Utilisez la commande suivante pour compiler le fichier main.typ :
    ```bash
    typst compile main.typ
    ```

6. Le fichier compilé sera généré dans le même répertoire sous le nom main.pdf


## Plus d'information dans le fichier readme.pdf