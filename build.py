import os
import subprocess
import re
import shutil

# Define the root directory
root_dir = "./"

# Find all main.typ files recursively
files = []
for foldername, subfolders, filenames in os.walk(root_dir):
    for filename in filenames:
        if filename == "main.typ":
            files.append(os.path.join(foldername, filename))

# Iterate over the found files
for file in files:
    if os.path.isfile(file):
        # Use grep and regex to extract uv_name and title_name from the file
        with open(file, "r") as f:
            content = f.read()
            uv_name_match = re.search(r'uv_name: "(.*?)"', content)
            title_name_match = re.search(r'title_name: "(.*?)"', content)

            if uv_name_match and title_name_match:
                uv_name = uv_name_match.group(1)
                title_name = title_name_match.group(1)

                # Replace illegal characters with underscores
                uv_name_clean = re.sub(r'[\\/:"*?<>|]', '', uv_name)
                title_name_clean = re.sub(r'[\\/:"*?<>|]', '', title_name)

                print(f"Compiling {file} => {uv_name_clean} - {title_name_clean}.pdf")

                # Check if typst executable exists
                if shutil.which("typst"):
                    subprocess.run(["typst", "compile", file, f"./{uv_name_clean} - {title_name_clean}.pdf", "--root", "./"])
                else:
                    print("Error: typst executable not found.")
            else:
                print(f"File: {file}, uv_name and/or title_name not found.")
