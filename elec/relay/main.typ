#import "../../template.typ": *

#show: template.with(
  uv_name: "Électronique Analogique",
  title_name: "Guide sur l'Utilisation des Relais",
  authors: ("Benoit THOMAS",),
)

#par(first-line-indent: 0pt)[

/ Temps: 10 minutes (lecture et réalisation)

/ Difficulté: facile

/ Matériel requis:
]

- Arduino UNO
- un Relais
- un Moteur
- une alimentation 9V

= Introduction
Le relais est un petit composant utilisé en électronique et qui est très utile dans des
montages Arduino. C’est un interrupteur pilotable par un signal électrique. Il est comparable
au transistor mais le relais fonctionne par un contact mécanique. En envoyant un signal de
5V, il peut mettre le contact dans un circuit de 230V, 10A, par exemple.

#grid(columns: 2,
[
  #set align(right)
  #image("images/Relay_open.svg", width: 50%)
],[
  #image("images/Relay_closed.svg", width: 50%)
],
)

Pour actionner le relais, il faut envoyer un faible courant dans la bobine qui va ensuite
mettre le contact.

= Objectif
Nous allons écrire un code pour commander l’allumage du moteur à partir de l’arduino. Pour
notre exemple l’objectif est d’allumer le moteur 5 secondes puis de l’éteindre 2 secondes, en
boucle.

#pagebreak(weak: true)
= Schéma et câblage
Pour commencer, nous allons câbler comme ci-dessous :

#image("images/relay_bb.svg")

La commande du relais est similaire à la commande d’une led. En effet pour établir le
contact il faut envoyer un signal au relais de la même façon qu’avec une led.


#code[
```cpp
// Définit le numéro de broche du relais
const int RELAY_PIN = 8;

void setup() {
  // Configure la broche du relais en tant que sortie
  pinMode(RELAY_PIN, OUTPUT);
}

void loop() {
  // Active le relais, alimentant ainsi le moteur
  digitalWrite(RELAY_PIN, HIGH);
  // Le moteur reste allumé pendant 5 secondes
  delay(5000);
  // Désactive le relais, coupant l'alimentation du moteur
  digitalWrite(RELAY_PIN, LOW);
  // Attends 2 secondes avant de répéter le processus
  delay(2000);
}
```
]

Ainsi, après avoir chargé le code, l’arduino ferme l’interrupteur 5 secondes, puis l’ouvre 2
secondes, en boucle.


= Conclusion 
En bref, le relais est un interrupteur électrique contrôlé par Arduino. Il permet de gérer des appareils puissants avec un faible signal de 5V.
