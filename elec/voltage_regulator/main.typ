#import "../../template.typ": *

#show: template.with(
  uv_name: "Électronique Analogique",
  title_name: "Créez une Alimentation Réglable",
)

#par(first-line-indent: 0pt)[

/ Temps: 20 minutes (lecture et réalisation)

/ Difficulté: Moyenne

/ Matériel requis:
]

  - Régulateur de tension 7805
  - Régulateur de tension LM317
  - Source d'alimentation non régulée (tension d'entrée)
  - Multimètre (facultatif)
  - Breadboard (plaque d'essai) et fils de raccordement


= Introduction

Les régulateurs de tension 7805 et LM317 sont des composants essentiels dans le domaine de l'électronique pour fournir une alimentation électrique stable à nos circuits. Le 7805 offre une tension de sortie fixe de 5 volts, tandis que le LM317 permet de créer une alimentation réglable avec une tension de sortie variable. Dans ce tutoriel, nous allons explorer comment utiliser ces deux régulateurs pour créer une alimentation électrique flexible et personnalisée.

Que ce soit pour alimenter des projets électroniques, des appareils, ou pour tester et prototyper, cette alimentation réglable sera un outil précieux dans votre boîte à outils électronique. Suivez ces étapes simples pour apprendre à construire votre propre alimentation électrique personnalisée.

= Montage du Circuit avec le 7805

+ Placez le régulateur de tension 7805 sur le breadboard.
+ Connectez la broche 1 (`Input`) du 7805 à la borne positive de votre source d'alimentation non régulée.
+ Connectez la broche 2 (`Common`) du 7805 à la masse (`GND`) de votre source d'alimentation.
+ Connectez la broche 3 (`Output`) du 7805 à une charge (par exemple, une LED avec une résistance de 220 ohms en série).

#align(center)[
  #image("./images/7805.svg", width: 30%)
]

= Alimentation et Test avec le 7805

+ Alimentez votre circuit en allumant votre source d'alimentation non régulée.
+ Vérifiez la tension de sortie à la broche 3 (`Output`) du 7805 à l'aide d'un multimètre (facultatif). Vous devriez mesurer une tension de sortie stable de 5 volts.

= Montage du Circuit avec le LM317

+ Connectez la borne positive de votre source d'alimentation non régulée à la 3ème pin (`Input`) du LM317.
  
+ Connectez la masse de votre source d'alimentation à la 3ème broche du potentiomètre.

+ Connectez la 1ère pin (`Adjust`) du LM317 à l'une des broches du potentiomètre.
+ Connectez la 2ème pin (`Output`)  LM317 à la deuxième broche du potentiomètre.

#align(center)[
  #image("./images/LM317.svg", width: 30%)
]

= Conclusion

Vous avez maintenant créé une alimentation réglable à l'aide des régulateurs de tension 7805 et LM317. Le 7805 fournit une tension fixe de 5 volts, tandis que le LM317 permet de régler la tension de sortie selon vos besoins. Ce projet peut être utilisé pour alimenter différents composants électroniques ou comme source d'alimentation pour vos projets.
