#import "../../template.typ": *

#show: template.with(
  uv_name: "Raspberry",
  title_name: "Surveillance de la Bande Passante",
)

#par(first-line-indent: 0pt)[
/ Temps: 5 minutes (lecture et application).

/ Difficulté: Facile.

/ Matériel: Raspberry Pi; Connexion Internet active
]

Lorsque vous utilisez une Raspberry Pi ou tout autre système GNU/Linux, il est utile de connaître le répertoire `/sys/class/net`. Ce répertoire fait partie d'un système de fichiers virtuel appelé sysfs, spécifique à GNU/Linux. Il joue un rôle essentiel en fournissant des informations et des réglages liés à vos interfaces réseau. Ces interfaces peuvent prendre différentes formes, qu'il s'agisse d'une connexion Ethernet câblée (comme eth0) ou d'une connexion Wi-Fi sans fil (comme wlan0).

De plus, à l'intérieur du répertoire `/sys/class/net/<INTERFACE>/statistics`, vous trouverez un sous-répertoire portant le nom de chaque interface réseau que vous avez. Ce sous-répertoire renferme des données importantes. Il contient une mine d'informations sous la forme de statistiques et de compteurs liés à cette interface particulière. Ces données sont collectées en permanence par le noyau Linux et fournissent un aperçu détaillé de l'utilisation de cette interface. Vous pouvez y trouver des informations telles que le nombre total d'octets reçus et envoyés, le nombre de paquets traités, les erreurs éventuelles, et bien plus encore.

Ce tutoriel vous guidera à travers l'utilisation de ces informations pour surveiller la bande passante de votre Raspberry Pi, vous permettant ainsi de mieux comprendre et gérer votre connexion réseau.

= Préparation

Assurez-vous que votre Raspberry Pi est correctement configurée avec Raspbian (ou un système d'exploitation similaire) et connectée à Internet. Vous pouvez également vous connecter à distance via SSH si vous préférez.

==== Ouvrir un Terminal
Sur votre Raspberry Pi, ouvrez un terminal. Vous pouvez le faire en utilisant l'interface graphique ou en vous connectant à distance via `SSH`.

==== Trouver les Interfaces Réseau
Utilisez la commande ls pour afficher les interfaces réseau disponibles. Tapez la commande suivante dans le terminal :

```bash
ls /sys/class/net
```

Cela affichera la liste des interfaces réseau disponibles, telles que `eth0`, `wlan0`, etc. Notez le nom de l'interface que vous souhaitez surveiller, car vous en aurez besoin plus tard dans le tutoriel.

==== Créer le Script
Copiez et collez le script suivant dans le terminal pour créer un nouveau fichier de script :

```bash
nano bandwidth_monitor.sh
```

Appuyez sur "Entrée" pour créer le fichier et ouvrir l'éditeur de texte Nano.

==== Copier le Code
Copiez le script Bash ci-dessous et collez-le dans l'éditeur Nano :


#code()[
```bash
#!/bin/bash

# Définir l'interface réseau que vous souhaitez surveiller (par exemple, eth0 pour Ethernet ou wlan0 pour le Wi-Fi)
INTERFACE="eth0"

# Initialiser des variables pour stocker les valeurs précédentes
PREV_RX=$(cat /sys/class/net/$INTERFACE/statistics/rx_bytes)
PREV_TX=$(cat /sys/class/net/$INTERFACE/statistics/tx_bytes)

while true; do
	# Obtenir les valeurs actuelles
	CURRENT_RX=$(cat /sys/class/net/$INTERFACE/statistics/rx_bytes)
	CURRENT_TX=$(cat /sys/class/net/$INTERFACE/statistics/tx_bytes)

	# Calculer les données transférées depuis la dernière vérification
	RX_DIFF=$((CURRENT_RX - PREV_RX))
	TX_DIFF=$((CURRENT_TX - PREV_TX))

	# Convertir les octets en unités lisibles par l'homme (par exemple, Ko, Mo, Go)
	RX_DIFF_KB=$((RX_DIFF / 1024))
	TX_DIFF_KB=$((TX_DIFF / 1024))

	# Afficher les résultats
	echo "Reçus : $RX_DIFF_KB Ko/s   Transmis : $TX_DIFF_KB Ko/s"

	# Mettre à jour les valeurs précédentes pour la prochaine itération
	PREV_RX=$CURRENT_RX
	PREV_TX=$CURRENT_TX

	# Attendre pendant un intervalle spécifié (par exemple, 1 seconde)
	sleep 1
done
```
]

Pour enregistrer le fichier dans Nano, appuyez sur `Ctrl+O`, puis appuyez sur "Entrée". Ensuite, pour quitter Nano, appuyez sur `Ctrl+X`.

==== Rendre le Script Exécutable
Exécutez la commande suivante pour rendre le script exécutable :

```bash
chmod +x bandwidth_monitor.sh
```

= Exécution du Script
Lancez le script en utilisant la commande suivante :

```bash
./bandwidth_monitor.sh
```

Le script commencera à surveiller la bande passante de l'interface réseau spécifiée.

#align(left)[
#code()[
  ```bash
    yaniss@raspberry ~> ./bandwidth_monitor.sh
      Reçus : 0 Ko      Transmis : 0 Ko
      Reçus : 1384 Ko   Transmis : 44 Ko
      Reçus : 1539 Ko   Transmis : 48 Ko
      Reçus : 1213 Ko   Transmis : 33 Ko
      Reçus : 1263 Ko   Transmis : 44 Ko
      Reçus : 1409 Ko   Transmis : 52 Ko
      Reçus : 1068 Ko   Transmis : 45 Ko
      Reçus : 1009 Ko   Transmis : 30 Ko
      Reçus : 872 Ko    Transmis : 27 Ko
    ```
  ]
]

Pour arrêter le script, appuyez sur `Ctrl+C` dans le terminal.

Félicitations ! Vous avez créé un script simple pour surveiller la bande passante de votre Raspberry Pi. Vous pouvez personnaliser ce script en modifiant l'interface réseau ou l'intervalle de surveillance selon vos besoins.
