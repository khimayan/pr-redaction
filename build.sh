#!/bin/bash

# Define the root directory
root_dir="./"

files=$(find . -type f -name 'main.typ')

# Iterate over the found files using a for loop
for file in $files; do
    if [ -f "$file" ]; then
        # Use grep and sed to extract uv_name and title_name from the file
        uv_name=$(grep -oP 'uv_name: "\K[^"]+' "$file")
        title_name=$(grep -oP 'title_name: "\K[^"]+' "$file")

        if [ -n "$uv_name" ] && [ -n "$title_name" ]; then
            # Replace illegal characters with underscores
            uv_name_clean=$(echo "$uv_name" | tr -d '\\/:\"*?<>|')
            title_name_clean=$(echo "$title_name" | tr -d '\\/:\"*?<>|')
            
            echo "Compiling $file => $uv_name_clean - $title_name_clean.pdf"
            typst compile $file "./$uv_name_clean - $title_name_clean.pdf" --root ./
        else
            echo "File: $file, uv_name and/or title_name not found."
        fi
    fi
done
